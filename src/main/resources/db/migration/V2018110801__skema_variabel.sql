
create table variable_root(
    id varchar(255),
    variable varchar(255),
    description varchar(3000),
    creator varchar(255),
    create_date DATETIME,
    approval varchar(255),
    approval_date DATETIME DEFAULT NULL,
    last_modifier varchar(255),
    modify_date DATETIME,
    flag varchar(2)
);

create table variable(
    id varchar(255),
    variable_root_id varchar(255),
    description varchar(3000),
    data_type int,
    max_length int,
    min_value real,
    max_value real,
    regex_value varchar(255),
    creator varchar(255),
    create_date DATETIME,
    approval varchar(255),
    approval_date DATETIME,
    last_modifier varchar(255),
    modify_date DATETIME,
    flag varchar(2)
);

create table question(
    id varchar(255),
    question_code varchar(255),
    label varchar(255),
    quest_text varchar(1000),
    instruction varchar(1000),
    creator varchar(255),
    create_date DATETIME,
    approval varchar(255),
    approval_date DATETIME,
    flag varchar(2),
    last_modifier varchar(70),
    modify_date DATETIME
);

create table question_variable(
    id varchar(255),
    question_id varchar(255),
    variable_id varchar(255),
    creator varchar(255),
    create_date DATETIME,
    flag varchar(2)
);

create table activity(
    id varchar(255),
    survey_code varchar(255),
    sektor varchar(255),
    seri varchar(255),
    nama varchar(255),
    tahun int,
    penyelenggara varchar(255),
    dana varchar(255),
    abstrak varchar(2000),
    tujuan varchar(2000),
    frekuensi varchar(255),
    predesesor varchar(255),
    create_date DATETIME,
    approval varchar(255),
    approval_date DATETIME,
    flag varchar(2),
    last_modifier varchar(70),
    modify_date DATETIME
);

create table question_variable_activity(
    id varchar(255),
    activity_id varchar(255),
    question_variable_id varchar(255),
    survey_id varchar(255),
    codelist_id varchar(200),
    registrar varchar(255),
    register_date DATETIME,
    flag varchar(2)
);

create table codelist(
    id varchar(255),
    classification varchar(200),
    code int NOT NULL,
    item varchar(200),
    creator varchar(70),
    create_date DATETIME,
    approval varchar(70),
    approval_date DATETIME,
    flag varchar(2),
    last_modifier varchar(70),
    modify_date DATETIME
);

create table codelist_qva(
    id varchar(255),
    qva_id varchar(255),
    codelist_id varchar(255),
    creator varchar(70),
    create_date DATETIME,
    flag varchar(2)
);

ALTER TABLE variable_root
    ADD CONSTRAINT variable_root_pkey PRIMARY KEY (id);

ALTER TABLE variable
    ADD CONSTRAINT variable_pkey PRIMARY KEY (id);

ALTER TABLE question
    ADD CONSTRAINT question_pkey PRIMARY KEY (id);

ALTER TABLE question_variable
    ADD CONSTRAINT question_variable_pkey PRIMARY KEY (id);

ALTER TABLE activity
    ADD CONSTRAINT activity_pkey PRIMARY KEY (id);

ALTER TABLE question_variable_activity
    ADD CONSTRAINT question_variable_activity_pkey PRIMARY KEY (id);

ALTER TABLE codelist
    ADD CONSTRAINT codelist_pkey PRIMARY KEY (id);

ALTER TABLE codelist_qva
    ADD CONSTRAINT codelist_qva_pkey PRIMARY KEY (id);

-- FK here

ALTER TABLE variable
    ADD CONSTRAINT fk_variable_1
    FOREIGN KEY (variable_root_id) REFERENCES variable_root(id);

ALTER TABLE question_variable
    ADD CONSTRAINT fk_question_variable_1
    FOREIGN KEY (question_id) REFERENCES question(id);

ALTER TABLE question_variable
    ADD CONSTRAINT fk_question_variable_2
    FOREIGN KEY (variable_id) REFERENCES variable(id);

ALTER TABLE question_variable_activity
    ADD CONSTRAINT fk_question_variable_activity_1
    FOREIGN KEY (activity_id) REFERENCES activity(id);

ALTER TABLE question_variable_activity
    ADD CONSTRAINT fk_question_variable_activity_2
    FOREIGN KEY (question_variable_id) REFERENCES question_variable(id);

ALTER TABLE codelist_qva
    ADD CONSTRAINT fk_codelist_qva_1
    FOREIGN KEY (qva_id) REFERENCES question_variable_activity(id);

ALTER TABLE codelist_qva
    ADD CONSTRAINT fk_codelist_qva_2
    FOREIGN KEY (codelist_id) REFERENCES codelist(id);

