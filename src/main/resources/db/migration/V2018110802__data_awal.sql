insert into variable_root
(id, variable,
    description,
    creator,
    create_date,
    approval,
    approval_date,
    last_modifier,
    modify_date,
    flag
 )
VALUES
('vr001', 'umur', 'umur responden', 'febi', '2018-09-17 5:31:00', 'febi', '2018-09-17 5:31:00', 'febi', '2018-09-17 5:31:00', 'A')
;

insert into variable
(
    id,
    variable_root_id  ,
    description,
    data_type,
    max_length,
    min_value,
    max_value,
    regex_value,
    creator  ,
    create_date ,
    approval  ,
    approval_date ,
    last_modifier  ,
    modify_date ,
    flag
 )
VALUES
('v001', 'vr001', 'umur krt', 1, 2, 0, 200, NULL,
'febi', '2018-09-17 5:31:00', 'febi', '2018-09-17 5:31:00', 'febi', '2018-09-17 5:31:00', 'A');

insert into question(id, question_code, label, quest_text, instruction, creator, create_date, approval, approval_date, flag, last_modifier, modify_date) VALUES
('q001', 'umur_krt', 'umur krt', 'Umur kepala rumah tangga', NULL, 'sosial', '2018-09-13 8:18:00', 'metodologi', '2018-09-13 20:18:00', 'A', 'metodologi', '2018-09-14 8:18:00');

insert into question_variable
(
    id,
    question_id,
    variable_id,
    creator,
    create_date,
    flag
 )
VALUES
('qv001', 'q001', 'v001', 'febi', '2018-09-17 5:31:00', 'A');

insert into activity(id, survey_code,	sektor,	seri,	nama,	tahun,	penyelenggara,	dana,	abstrak,	tujuan,	frekuensi ,	predesesor,	create_date,	approval,	approval_date,	flag,	last_modifier,	modify_date) VALUES
('a001', 'sak16ags', 'Sosial', 'Sakernas',	'Survei Angkatan Kerja Nasional  2017',	2016, 'BPS', 'APBN 2017', 'Data ketenagakerjaan yang dikumpulkan oleh Badan Pusat Statistik (BPS) melalui sensus dan survei antara lain: Sensus Penduduk (SP), Survei Penduduk Antar Sensus (Supas), Survei Sosial Ekonomi Nasional (Susenas) dan Survei Angkatan Kerja Nasional (Sakernas). Dari surveisurvei tersebut, hanya Sakernas yang dirancang khusus untuk mengumpulkan data yang dapat menggambarkan keadaan umum ketenagakerjaan antar periode pencacahan. Hingga saat ini, Sakernas mengalami berbagai perubahan baik waktu pelaksanaan, level estimasi, cakupan, maupun metodologi.',	'menyediakan data pokok ketenagakerjaan yang berkesinambungan. Secara khusus, untuk memperoleh estimasi data jumlah penduduk bekerja, jumlah pengangguran, dan indikator ketenagakerjaan lainnya serta perkembangannya di tingkat provinsi maupun nasional.',	'Semesteran', 'sak16feb' , '2018-09-13 8:18:00', 'metodologi', '2018-09-13 20:18:00', 'A', 'metodologi', '2018-09-14 8:18:00'),
('a002', 'sak17feb', 'Sosial', 'Sakernas',	'Survei Angkatan Kerja Nasional  2017',	2016, 'BPS', 'APBN 2017', 'Data ketenagakerjaan yang dikumpulkan oleh Badan Pusat Statistik (BPS) melalui sensus dan survei antara lain: Sensus Penduduk (SP), Survei Penduduk Antar Sensus (Supas), Survei Sosial Ekonomi Nasional (Susenas) dan Survei Angkatan Kerja Nasional (Sakernas). Dari surveisurvei tersebut, hanya Sakernas yang dirancang khusus untuk mengumpulkan data yang dapat menggambarkan keadaan umum ketenagakerjaan antar periode pencacahan. Hingga saat ini, Sakernas mengalami berbagai perubahan baik waktu pelaksanaan, level estimasi, cakupan, maupun metodologi.',	'menyediakan data pokok ketenagakerjaan yang berkesinambungan. Secara khusus, untuk memperoleh estimasi data jumlah penduduk bekerja, jumlah pengangguran, dan indikator ketenagakerjaan lainnya serta perkembangannya di tingkat provinsi maupun nasional.',	'Semesteran', 'sak16feb' , '2018-09-13 8:18:00', 'metodologi', '2018-09-13 20:18:00', 'A', 'metodologi', '2018-09-14 8:18:00');

insert into question_variable_activity(
    id,
    activity_id,
    question_variable_id,
    survey_id,
    codelist_id,
    registrar,
    register_date,
    flag
)
VALUES
('qva001', 'a001', 'qv001', '-', '-', 'akmal', '2018-09-17 5:31:00', 'A');

insert into codelist(id, classification, code, item, creator, approval, flag, last_modifier) VALUES
('c001', 'umur',	1,	'<=5', 'sosial', 'metodologi', 'A', 'metodologi'),
('c002', 'umur',	2,	'[6,10]', 'sosial', 'metodologi', 'A', 'metodologi');

insert into codelist_qva(
    id,
    qva_id,
    codelist_id,
    creator,
    create_date,
    flag
) values
('cqva001', 'qva001', 'c001', 'akmal', '2018-09-17 5:31:00', 'A'),
('cqva002', 'qva001', 'c002', 'akmal', '2018-09-17 5:31:00', 'A');