package id.go.bps.sm.metadata.dao;

import id.go.bps.sm.metadata.entity.Variable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface VariableDao extends PagingAndSortingRepository<Variable, String> {
    @Query("select v from Variable v where v.variableRoot.variable like %?1")
    Page<Variable> findByVariableName(String variable, Pageable pageable);
}