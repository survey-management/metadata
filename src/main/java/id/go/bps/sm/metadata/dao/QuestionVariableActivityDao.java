package id.go.bps.sm.metadata.dao;

import id.go.bps.sm.metadata.entity.QuestionVariableActivity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface QuestionVariableActivityDao extends PagingAndSortingRepository<QuestionVariableActivity, String> {
    
}