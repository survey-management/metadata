package id.go.bps.sm.metadata.entity;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="activity")
public class Activity implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Getter
    @Setter
    public String id;

    @Getter @Setter
    @Column(name = "survey_code")
    private String surveyCode;

    @Getter @Setter
    private String sektor;

    @Getter @Setter
    private String seri;

    @Getter @Setter
    private String nama;

    @Getter @Setter
    private int tahun;

    @Getter @Setter
    private String penyelenggara;

    @Getter @Setter
    private String dana;

    @Getter @Setter
    private String abstrak;

    @Getter @Setter
    private String tujuan;

    @Getter @Setter
    private String frekuensi;

    @Getter @Setter
    private String predesesor;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "create_date")
    private Date createDate;

    @Getter @Setter
    private String approval;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "approval_date")
    private Date approvalDate;

    @Getter @Setter
    private String flag;


    @Getter @Setter
    @Column(name = "last_modifier")
    private String lastModifier;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "modify_date")
    private Date modifyDate;

}
