package id.go.bps.sm.metadata.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="variable_root")
public class VariableRoot implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Getter
    @Setter
    public String id;

    @Getter
    @Setter
    public String variable;

    @Getter
    @Setter
    public String description;

    @Getter
    @Setter
    public String creator;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "create_date")
    private Date createDate;

    @Getter @Setter
    private String approval;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "approval_date")
    private Date approvalDate;

    @Getter @Setter
    @Column(name = "last_modifier")
    private String lastModifier;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "modify_date")
    private Date modifyDate;

    @Getter @Setter
    private String flag;
}
