package id.go.bps.sm.metadata.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "variable")
public class Variable implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Getter
    @Setter
    public String id;

//    @Getter
//    @Setter
//    @Column(name = "variable_root_id")
//    public String variableRootId;

    @Getter
    @Setter
    public String description;

    @Getter
    @Setter
    @Column(name = "data_type")
    public Integer dataType;

    @Getter
    @Setter
    @Column(name = "max_length")
    public Integer maxLength;

    @Getter
    @Setter
    @Column(name = "min_value")
    public Double minValue;

    @Getter
    @Setter
    @Column(name = "max_value")
    public Double maxValue;

    @Getter
    @Setter
    @Column(name = "regex_value")
    public String regexValue;

    @Getter
    @Setter
    public String creator;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "create_date")
    private Date createDate;

    @Getter @Setter
    private String approval;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "approval_date")
    private Date approvalDate;

    @Getter @Setter
    @Column(name = "last_modifier")
    private String lastModifier;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "modify_date")
    private Date modifyDate;

    @Getter @Setter
    private String flag;


    @ManyToOne
    @JoinColumn(name="variable_root_id")
    private VariableRoot variableRoot;

}
