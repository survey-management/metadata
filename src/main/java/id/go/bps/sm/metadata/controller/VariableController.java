package id.go.bps.sm.metadata.controller;

import id.go.bps.sm.metadata.dao.VariableDao;
import id.go.bps.sm.metadata.entity.Variable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class VariableController {
    @Autowired
    private VariableDao variableDao;

    @GetMapping(value = "/var")
    public Page<Variable> findVariableByPage(
            @RequestParam(name = "variable", required = false) String variable,
            Pageable page)
    {
        if(variable!=null)
            return variableDao.findByVariableName(variable, page);
        else
            return variableDao.findAll(page);
    }

}
