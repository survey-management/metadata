package id.go.bps.sm.metadata.dao;

import id.go.bps.sm.metadata.entity.QuestionVariable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface QuestionVariableDao extends PagingAndSortingRepository<QuestionVariable, String> {
    
}