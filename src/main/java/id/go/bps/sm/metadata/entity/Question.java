package id.go.bps.sm.metadata.entity;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="question")
public class Question implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Getter
    @Setter
    public String id;

    @Getter @Setter
    @Column(name = "question_code")
    private String questionCode;

    @Getter @Setter
    private String label;

    @Getter @Setter
    @Column(name = "quest_text")
    private String questText;

    @Getter @Setter
    private String instruction;

    @Getter @Setter
    private String creator;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "create_date")
    private Date createDate;

    @Getter @Setter
    private String approval;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "approval_date")
    private Date approvalDate;

    @Getter @Setter
    private String flag;

    @Getter @Setter
    @Column(name = "last_modifier")
    private String lastModifier;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "modify_date")
    private Date modifyDate;

}
