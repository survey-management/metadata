package id.go.bps.sm.metadata.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "question_variable_activity")
public class QuestionVariableActivity implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Getter
    @Setter
    public String id;

    @Getter @Setter
    @Column(name = "activity_id")
    public String activityId;

    @Getter @Setter
    @Column(name = "question_variable_id")
    public String questionVariableId;

    @Getter @Setter
    @Column(name = "survey_id")
    public String surveyId;

    @Getter @Setter
    @Column(name = "codelist_id")
    public String codelistId;

    @Getter @Setter
    public String registrar;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "register_date")
    public Date registerDate;

    @Getter @Setter
    public String flag;
}
