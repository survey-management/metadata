package id.go.bps.sm.metadata.dao;

import id.go.bps.sm.metadata.entity.Codelist;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CodelistDao extends PagingAndSortingRepository<Codelist, String> {
}