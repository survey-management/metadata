package id.go.bps.sm.metadata.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "codelist_variable_root")
public class CodelistVariableRoot implements Serializable{

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Getter
    @Setter
    public String id;

    @Getter @Setter
    @Column(name = "variable_root_id")
    public String variableRootId;

    @Getter @Setter
    @Column(name = "codelist_id")
    public String codelistId;

    @Getter @Setter
    public String creator;

    @DateTimeFormat(pattern = "dd-mm-yyyy hh24:mi:ss")
    @Getter @Setter
    @Column(name = "create_date")
    public Date createDate;

    @Getter @Setter
    public String flag;

}
