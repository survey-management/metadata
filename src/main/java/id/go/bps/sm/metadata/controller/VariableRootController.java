package id.go.bps.sm.metadata.controller;

import id.go.bps.sm.metadata.dao.VariableDao;
import id.go.bps.sm.metadata.dao.VariableRootDao;
import id.go.bps.sm.metadata.entity.Variable;
import id.go.bps.sm.metadata.entity.VariableRoot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class VariableRootController {
    @Autowired
    private VariableRootDao variableRootDao;

    @GetMapping(value = "/varroot")
    public Page<VariableRoot> findVariableRoot(
            @RequestParam(name = "variable", required = false) String variable,
            @PageableDefault(size = 10) Pageable page)
    {
        if(variable != null)
            return variableRootDao.findByVariableContainingIgnoreCase(variable, page);
        else
            return variableRootDao.findAll(page);
    }

}
