package id.go.bps.sm.metadata.dao;

import id.go.bps.sm.metadata.entity.Question;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface QuestionDao extends PagingAndSortingRepository<Question, String> {
    
}